#include "RingBuffer.h"

#define AMOUNT_TEST_1 21
#define AMOUNT_TEST_2 33

unsigned int TestRingBuffer1(void);
unsigned int TestRingBuffer2(void);

int main() {
  int i;
  unsigned int errors = 0;

  for (i = 0; i < AMOUNT_TEST_1; i++) {
    errors += TestRingBuffer1();
  }

  for (i = 0; i < AMOUNT_TEST_2; i++) {
    errors += TestRingBuffer2();
  }

  return 0;
}

/**
 * Function: TestRingBuffer1
 * Description: Write 1 Byte and read 1 Byte,
 *				check the result.
 *				Repeat this for 3 times
 *				of the size of the RingBuffer.
 */
unsigned int TestRingBuffer1(void) {
  unsigned int errorCount = 0;
  unsigned char bufferSize = 59;
  TRingBuffer ringBuffer = RingBufferCreate(bufferSize);

  int i;
  unsigned char count = 1;
  unsigned char read = 0;

  for (i = 0; i < 3 * bufferSize; i++, count++) {
    if (!RingBufferWrite(ringBuffer, count))
      errorCount++;

    if (!RingBufferRead(ringBuffer, &read))
      errorCount++;

    if (read != count)
      errorCount++;

    read = 0;
  }

  RingBufferDestroy(ringBuffer);

  return errorCount;
}

/**
 * Function: TestRingBuffer2
 * Description: Write Bytes until the buffer is full and read all Bytes,
 *				check the result.
 *				Repeat this for so many times
 *				as the size of the RingBuffer.
 */
unsigned int TestRingBuffer2(void) {
  unsigned int errorCount = 0;
  unsigned char bufferSize = 21;
  TRingBuffer ringBuffer = RingBufferCreate(bufferSize);

  int i;
  int j;
  unsigned char read = 0;

  for (j = 0; j < bufferSize; j++) {
    for (i = 1; i < bufferSize; i++) {
      if (!RingBufferWrite(ringBuffer, i))
        errorCount++;
    }

    read = 0;

    for (i = 1; i < bufferSize; i++) {
      if (!RingBufferRead(ringBuffer, &read))
        errorCount++;

      if (read != i)
        errorCount++;

      read = 0;
    }
  }

  RingBufferDestroy(ringBuffer);

  return errorCount;
}
